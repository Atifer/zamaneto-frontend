import Vue from 'vue'

// Lib imports
import axios from 'axios'
import router from '@/router'

axios.defaults.baseURL = 'https://api.zamaneto.ir'
// axios.defaults.baseURL = 'http://10.42.0.119:8000'
Vue.prototype.$http = axios
axios.interceptors.response.use(function (response) {
  return response
}, function (error) {
  if (error.response.status === 401) {
    router.push('login')
  } else {
    return Promise.reject(error)
  }
})
