import Vue from 'vue'
import moment from 'moment'
import 'moment/locale/fa'
import VueMoment from 'vue-moment'

Vue.use(VueMoment, {
  moment
})
