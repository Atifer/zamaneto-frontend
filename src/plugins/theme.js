export default {
  // primary: '#4caf50',
  // secondary: '#4caf50',
  // tertiary: '#495057',
  // accent: '#82B1FF',
  // error: '#f55a4e',
  // info: '#00d3ee',
  // success: '#5cb860',
  // warning: '#ffa21a',
  // primary: '#CA65E3',
  primary: '#5a8df2',
  secondary: '#FFA66C',
  tertiary: '#60E2C9',
  accent: '#82B1FF',
  error: '#f55a4e',
  info: '#00d3ee',
  success: '#5cb860',
  warning: '#ffa21a'
}
