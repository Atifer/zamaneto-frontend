import Vue from 'vue'
import VeeValidateLocale from '@/lang/fa/VeeValidate'
import fa from 'vee-validate/dist/locale/fa'
import {
  ValidationObserver,
  ValidationProvider,
  withValidation,
  Validator
} from 'vee-validate'

Vue.component('ValidationObserver', ValidationObserver)
Vue.component('ValidationProvider', ValidationProvider)
Vue.component('withValidation', withValidation)
Validator.localize('fa', fa)
Validator.localize(VeeValidateLocale)

Validator.extend('verify_password', {
  // getMessage: field => `The password must contain at least: 1 uppercase letter, 1 lowercase letter, 1 number, and one special character (E.g. , . _ & ? etc)`,
  // Must set right i18n for this getMessage
  getMessage: field => field + ' حداقل باید شامل ۱ حرف بزرگ، ۱ حرف کوچک، ۱ عدد و ۱ نویسه ویژه مانند ! باشد.',
  validate: value => {
    var strongRegex = new RegExp('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.{8,})')
    return strongRegex.test(value)
  }
})

Validator.extend('duration', {
  validate: value => {
    var regex = new RegExp('^([0-9]{2,3}):([0-5][0-9]):([0-5][0-9])$')
    return regex.test(value)
  }
})

Validator.extend('dayTime', {
  validate: value => {
    var regex = new RegExp('^([0-2][0-9]):([0-5][0-9]):([0-5][0-9])$')
    if (regex.test(value)) {
      var hourStr = value.split(':')[0]
      var hourNum = Number(hourStr)
      if (hourNum < 24) {
        return true
      }
    }
    return false
  }
})
