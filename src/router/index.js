import VueRouter from 'vue-router'
import routes from './routes'
import store from '@/store'

// Remember me functionality
if (window.localStorage) {
  let token = window.localStorage.getItem('token')
  if (token === '' || token === null || token === undefined) {
    store.commit('user/setToken', '')
  } else {
    store.commit('user/setToken', token)
  }
}

// configure router
const router = new VueRouter({
  routes, // short for routes: routes
  mode: 'history',
  linkActiveClass: 'active',
  scrollBehavior (to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition
    } else {
      return { x: 0, y: 0 }
    }
  }
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    // this route requires auth, check if logged in
    // if not, redirect to login page.
    if (!store.state.user.token) {
      next({
        path: '/login',
        query: {
          redirect: to.fullPath
        }
      })
    } else {
      next()
    }
  } else {
    next() // make sure to always call next()!
  }
})

export default router
