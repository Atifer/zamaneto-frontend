import DashboardLayout from '@/layout/DashboardLayout.vue'
import DefaultLayout from '@/layout/DefaultLayout.vue'
// GeneralViews
import NotFound from '@/views/NotFoundPage.vue'
// Default pages
const Home = () =>
  import(/* webpackChunkName: "pages" */ '@/views/Default/Home.vue')
const Login = () =>
  import(/* webpackChunkName: "pages" */ '@/views/Default/Login.vue')
const Register = () =>
  import(/* webpackChunkName: "pages" */ '@/views/Default/Register.vue')
const ForgotPassword = () =>
  import(/* webpackChunkName: "pages" */ '@/views/Default/ForgotPassword.vue')
const ResetPassword = () =>
  import(/* webpackChunkName: "pages" */ '@/views/Default/ResetPassword.vue')
const Verify = () =>
  import(/* webpackChunkName: "pages" */ '@/views/Default/Verify.vue')

// Dashboard pages
const Dashboard = () =>
  import(/* webpackChunkName: "dashboard" */ '@/views/Dashboard/Dashboard.vue')
const TimeTracker = () =>
  import(/* webpackChunkName: "dashboard" */ '@/views/Dashboard/TimeTracker.vue')
const Project = () =>
  import(/* webpackChunkName: "dashboard" */ '@/views/Dashboard/Project.vue')
const Team = () =>
  import(/* webpackChunkName: "dashboard" */ '@/views/Dashboard/Team/Index.vue')
const MemberManagement = () =>
  import(/* webpackChunkName: "dashboard" */ '@/views/Dashboard/Team/MemberManagement.vue')
const Tag = () =>
  import(/* webpackChunkName: "dashboard" */ '@/views/Dashboard/Tag.vue')
const UserProfile = () =>
  import(/* webpackChunkName: "dashboard" */ '@/views/Dashboard/UserProfile.vue')
const Report = () =>
  import(/* webpackChunkName: "dashboard" */ '@/views/Dashboard/Report.vue')
let defaultPages = {
  path: '/',
  component: DefaultLayout,
  name: 'Default',
  redirect: '/time-tracker',
  children: [
    {
      path: '/home',
      name: 'Home',
      components: { default: Home }
    },
    {
      path: '/login',
      name: 'Login',
      components: { default: Login }
    },
    {
      path: '/register',
      name: 'Register',
      components: { default: Register }
    },
    {
      path: '/resetpassword',
      name: 'ResetPassword',
      components: { default: ResetPassword }
    },
    {
      path: '/forgotpassword',
      name: 'ForgotPassword',
      components: { default: ForgotPassword }
    },
    {
      path: '/verify',
      name: 'Verify',
      components: { default: Verify }
    }
  ]
}

let dashboardPages = {
  path: '/dashboard',
  component: DashboardLayout,
  name: 'dboard',
  redirect: '/dashboard',
  children: [
    {
      path: '/dashboard',
      name: 'Dashboard',
      components: { default: Dashboard },
      meta: { requiresAuth: true }
    },
    {
      path: '/time-tracker',
      name: 'TimeTracker',
      components: { default: TimeTracker },
      meta: { requiresAuth: true }
    },
    {
      path: '/team',
      name: 'Team',
      components: { default: Team },
      meta: { requiresAuth: true }
    },
    {
      path: '/team/member-management/:teamId',
      name: 'MemberManagement',
      components: { default: MemberManagement },
      meta: { requiresAuth: true }
    },
    {
      path: '/project',
      name: 'Project',
      components: { default: Project },
      meta: { requiresAuth: true }
    },
    {
      path: '/report',
      name: 'Report',
      components: { default: Report },
      meta: { requiresAuth: true }
    },
    {
      path: '/tag',
      name: 'Tag',
      components: { default: Tag },
      meta: { requiresAuth: true }
    },
    {
      path: '/user-profile',
      name: 'UserProfile',
      components: { default: UserProfile },
      meta: { requiresAuth: true }
    }
  ]
}

const routes = [
  defaultPages,
  dashboardPages,
  { path: '*', component: NotFound }
]

export default routes
