import axios from 'axios'

export default {
  async addProject ({ rootState, commit }, project) {
    let config = {
      headers: { 'Authorization': 'Token ' + rootState.user.token }
    }
    return axios.post('/project/', project, config)
  },
  async getProjectList ({ rootState, commit }) {
    let config = {
      headers: { 'Authorization': 'Token ' + rootState.user.token }
    }
    return axios.get('/project/', config).then(response => {
      commit('setProjectList', response.data)
      commit('setProjectListDataTable', response.data)
      commit('setTotalProject', response.data.length)
    })
  },
  async projectPaginationHandler ({ state, commit }) {
    return new Promise((resolve, reject) => {
      const { sortBy, descending } = state.projectPagination
      let items = state.projectList

      if (sortBy) {
        items = items.sort((a, b) => {
          const sortA = a[sortBy]
          const sortB = b[sortBy]

          if (descending) {
            if (sortA < sortB) return 1
            if (sortA > sortB) return -1
            return 0
          } else {
            if (sortA < sortB) return -1
            if (sortA > sortB) return 1
            return 0
          }
        })
      }

      commit('setProjectListDataTable', items)
      resolve()
    }, 1000)
  },
  async deleteProject ({ rootState, commit }, projectId) {
    let config = {
      headers: { 'Authorization': 'Token ' + rootState.user.token }
    }
    return axios.delete(`/project/${projectId}/`, config)
  }
}
