export default {
  projectList: null,
  projectPagination: {
    descending: true,
    page: 1,
    rowsPerPage: 5,
    sortBy: 'start',
    totalItems: 5
  },
  projectListDataTable: null
}
