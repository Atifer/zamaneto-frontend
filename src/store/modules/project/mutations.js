import { set } from '@/utils/vuex'

export default {
  setProjectList: set('projectList'),
  setProjectPagination: set('projectPagination'),
  setProjectListDataTable: set('projectListDataTable'),
  setTotalProject (state, totalProject) {
    state.projectPagination.totalItems = totalProject
  }
}
