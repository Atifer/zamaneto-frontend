import axios from 'axios'

export default {
  async addTeam ({ rootState, commit }, team) {
    let config = {
      headers: { 'Authorization': 'Token ' + rootState.user.token }
    }
    return axios.post('/team/', team, config)
  },
  async addMember ({ rootState, commit }, users) {
    let config = {
      headers: { 'Authorization': 'Token ' + rootState.user.token }
    }
    return axios.post('/team/', users, config)
  },
  async getTeamDetails ({ rootState, commit }, teamId) {
    let config = {
      headers: { 'Authorization': 'Token ' + rootState.user.token }
    }
    return axios.get(`/team/${teamId}`, config).then(response => {
      commit('setTeamDetails', response.data)
      commit('setLevelOneMemberList', response.data.level1)
      commit('setLevelTwoMemberList', response.data.level2)
    })
  },
  async getTeamList ({ rootState, commit }) {
    let config = {
      headers: { 'Authorization': 'Token ' + rootState.user.token }
    }
    return axios.get('/team/', config).then(response => {
      commit('setTeamList', response.data)
      commit('setTeamListDataTable', response.data)
      commit('setTotalTeam', response.data.length)
    })
  },
  async getUserList ({ rootState, commit }) {
    let config = {
      headers: { 'Authorization': 'Token ' + rootState.user.token }
    }
    return axios.get('/users/', config).then(response => {
      let users = response.data.map(user => {
        return user.username
      })
      commit('setUserList', users)
    })
  },
  async submitTeamMember ({ rootState, commit }, team) {
    let config = {
      headers: { 'Authorization': 'Token ' + rootState.user.token }
    }
    return axios.patch(`/team/${team.id}/`, team, config)
  },
  async teamPaginationHandler ({ state, commit }) {
    return new Promise((resolve, reject) => {
      const { sortBy, descending } = state.teamPagination
      let items = state.teamList

      if (sortBy) {
        items = items.sort((a, b) => {
          const sortA = a[sortBy]
          const sortB = b[sortBy]

          if (descending) {
            if (sortA < sortB) return 1
            if (sortA > sortB) return -1
            return 0
          } else {
            if (sortA < sortB) return -1
            if (sortA > sortB) return 1
            return 0
          }
        })
      }

      commit('setTeamListDataTable', items)
      resolve()
    }, 1000)
  },
  async deleteTeam ({ rootState, commit }, teamId) {
    let config = {
      headers: { 'Authorization': 'Token ' + rootState.user.token }
    }
    return axios.delete(`/team/${teamId}/`, config)
  }
}
