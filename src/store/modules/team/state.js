export default {
  teamList: null,
  teamPagination: {
    descending: true,
    page: 1,
    rowsPerPage: 5,
    sortBy: 'start',
    totalItems: 5
  },
  teamListDataTable: null,
  teamDetails: null,
  levelOneMemberList: [],
  levelTwoMemberList: [],
  userList: null
}
