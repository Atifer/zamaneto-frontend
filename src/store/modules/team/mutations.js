import { set } from '@/utils/vuex'

export default {
  setTeamList: set('teamList'),
  setTeamPagination: set('teamPagination'),
  setTeamListDataTable: set('teamListDataTable'),
  setTotalTeam (state, totalTeam) {
    state.teamPagination.totalItems = totalTeam
  },
  setTeamDetails: set('teamDetails'),
  setLevelOneMemberList: set('levelOneMemberList'),
  setLevelTwoMemberList: set('levelTwoMemberList'),
  setUserList: set('userList')
}
