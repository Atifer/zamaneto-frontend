import { set } from '@/utils/vuex'

export default {
  setToken: set('token'),
  setInfo: set('info')
  // toggleDrawer: toggle('drawer')
}
