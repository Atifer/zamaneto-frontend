import axios from 'axios'

export default {
  async login ({ commit }, auth) {
    return axios.post('/rest-auth/login/', auth).then(response => {
      commit('setToken', response.data.key)
    })
  },
  async getUserInfo ({ state, commit }) {
    let config = {
      headers: { 'Authorization': 'Token ' + state.token }
    }
    return axios.get('/rest-auth/user/', config).then(response => {
      commit('setInfo', response.data)
    })
  },
  async register ({ commit }, auth) {
    return axios.post('/rest-auth/registration/', auth)
  },
  async verify ({ commit }, key) {
    return axios.post('/rest-auth/registration/verify-email/', { key: key })
  },
  async logout ({ state, commit }) {
    let config = {
      headers: { 'Authorization': 'Token ' + state.token }
    }
    return axios.post('/rest-auth/logout/', {}, config).then(() => {
      commit('setToken', '')
      var StartTime = window.localStorage.getItem('StartTime')
      window.localStorage.clear()
      if (StartTime != null) {
        window.localStorage.setItem('StartTime', StartTime)
      }
    })
  },
  async changePassword ({ rootState }, passModel) {
    let config = {
      headers: { 'Authorization': 'Token ' + rootState.user.token }
    }
    return axios.post('/rest-auth/password/change/', passModel, config)
  },
  async resetRequest ({ commit }, email) {
    return axios.post('/rest-auth/password/reset/', { email: email })
  },
  async expireCheck ({ commit }, tokens) {
    return axios.get(`/rest-auth/password/reset/confirm/${tokens.uid}/${tokens.token}`)
  },
  async resetPassword ({commit } , configs) {
    const pass1 = configs.pass1;
    const pass2 = configs.pass2;
    const data = { new_password1 : pass1 , new_password2 : pass2 };

    return axios.post(`/rest-auth/password/reset/confirm/${configs.uid}/${configs.token}`, data)
  },
  async UpdateProfile ({ rootState }, userModel) {
    let config = {
      headers: { 'Authorization': 'Token ' + rootState.user.token }
    }
    return axios.put('rest-auth/user/', userModel, config)
  }
}
