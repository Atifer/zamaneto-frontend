import { set } from '@/utils/vuex'

export default {
  setWorklogList: set('worklogList'),
  setWorklogPagination: set('worklogPagination'),
  setWorklogListDataTable: set('worklogListDataTable'),
  setTotalWorklog (state, totalWorklog) {
    state.worklogPagination.totalItems = totalWorklog
  },
  setIsTimerStart: set('isTimerStart')
}
