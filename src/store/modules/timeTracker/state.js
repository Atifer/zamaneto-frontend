export default {
  worklogList: null,
  worklogPagination: {
    descending: true,
    page: 1,
    rowsPerPage: 25,
    sortBy: 'start',
    totalItems: 5
  },
  worklogListDataTable: null,
  isTimerStart: false
}
