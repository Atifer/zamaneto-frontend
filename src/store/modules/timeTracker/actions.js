import axios from 'axios'
export default {
  // async startWatch( { rootState }, model) {
  //   let config = {
  //     headers: { 'Authorization' : 'Token ' + rootState.user.token }
  //   }
  //   axios.post('/period/', model , config).then(r => {
  //     window.localStorage.setItem('StartedPeriod', r.data.id);
  //   });
  // },
  // async GetPeriodStartTime( { rootState } , id) {
  //   let config = {
  //     headers: { 'Authorization' : 'Token ' + rootState.user.token }
  //   }
  //   axios.get('/period/' + id , config).then((r) => {

  //   })
  // },
  async durationModelWorklog ({ rootState }, durationModel) {
    let config = {
      headers: { 'Authorization': 'Token ' + rootState.user.token }
    }
    return axios.post('/period/', durationModel, config)
  },
  async updateLastPeriod ({ rootState }, durationModel) {
    let config = {
      headers: { 'Authorization': 'Token ' + rootState.user.token }
    }
    return axios.put('/period/', durationModel, config)
  },
  async startEndModelWorklog ({ rootState }, startEndModel) {
    let config = {
      headers: { 'Authorization': 'Token ' + rootState.user.token }
    }
    return axios.post('/period/', startEndModel, config)
  },
  async startTimerModelWorklog ({ rootState, commit }, timerModel) {
    let config = {
      headers: { 'Authorization': 'Token ' + rootState.user.token }
    }
    return axios.post('/period/', timerModel, config).then(() => {
      commit('setIsTimerStart', true)
      window.localStorage.setItem('isTimerStart', JSON.stringify(true))
    })
  },
  async stopTimerModelWorklog ({ rootState, commit }) {
    let config = {
      headers: { 'Authorization': 'Token ' + rootState.user.token }
    }
    return axios.put('/period/', {}, config).then(() => {
      commit('setIsTimerStart', false)
      window.localStorage.setItem('isTimerStart', JSON.stringify(false))
    })
  },
  async editWorklog ({ rootState }, model) {
    let config = {
      headers: { 'Authorization': 'Token ' + rootState.user.token }
    }
    return axios.put(`/period/${model.id}/`, model, config)
  },
  async deleteWorklog ({ rootState, commit }, worklogId) {
    let config = {
      headers: { 'Authorization': 'Token ' + rootState.user.token }
    }
    return axios.delete(`/period/${worklogId}/`, config)
  },
  async getWorklogList ({ rootState, commit }) {
    let config = {
      headers: { 'Authorization': 'Token ' + rootState.user.token }
    }
    return axios.get('/period/', config).then(response => {
      commit('setWorklogList', response.data)
      commit('setWorklogListDataTable', response.data)
      commit('setTotalWorklog', response.data.length)
    })
  },
  async getLastPeriod ({ rootState }) {
    let config = {
      headers: { 'Authorization': 'Token ' + rootState.user.token }
    }
    return axios.get('/last-period/', config)
  },
  async worklogPaginationHandler ({ state, commit }) {
    return new Promise((resolve, reject) => {
      const { sortBy, descending } = state.worklogPagination
      let items = state.worklogList

      if (sortBy) {
        items = items.sort((a, b) => {
          const sortA = a[sortBy]
          const sortB = b[sortBy]

          if (descending) {
            if (sortA < sortB) return 1
            if (sortA > sortB) return -1
            return 0
          } else {
            if (sortA < sortB) return -1
            if (sortA > sortB) return 1
            return 0
          }
        })
      }

      commit('setWorklogListDataTable', items)
      resolve()
    }, 1000)
  }
}
