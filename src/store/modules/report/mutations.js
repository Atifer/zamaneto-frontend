import { set } from '@/utils/vuex'

export default {
  setReportList: set('reportList'),
  setReportPagination: set('reportPagination'),
  setReportListDataTable: set('reportListDataTable'),
  setTotalReport (state, totalReport) {
    state.reportPagination.totalItems = totalReport
  },
  setIsTimerStart: set('isTimerStart'),
  resetReport (state) {
    state.setReportList = null
    state.setReportListDataTable = null
    state.setTotalReport = 5
  }
}
