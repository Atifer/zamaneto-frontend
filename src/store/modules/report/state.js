export default {
  reportList: null,
  reportPagination: {
    descending: true,
    page: 1,
    rowsPerPage: 5,
    sortBy: 'start',
    totalItems: 5
  },
  reportListDataTable: null,
  isTimerStart: false
}
