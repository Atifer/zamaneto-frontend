import axios from 'axios'

export default {
  async fetchReport ({ rootState, commit }, range) {
    let config = {
      headers: { 'Authorization': 'Token ' + rootState.user.token }
    }
    return axios.post('/report/', range, config).then(response => {
      commit('setReportList', response.data)
      commit('setReportListDataTable', response.data)
      commit('setTotalReport', response.data.length)
    })
  },
  async reportPaginationHandler ({ state, commit }) {
    return new Promise((resolve, reject) => {
      const { sortBy, descending } = state.reportPagination
      let items = state.reportList

      if (sortBy) {
        items = items.sort((a, b) => {
          const sortA = a[sortBy]
          const sortB = b[sortBy]

          if (descending) {
            if (sortA < sortB) return 1
            if (sortA > sortB) return -1
            return 0
          } else {
            if (sortA < sortB) return -1
            if (sortA > sortB) return 1
            return 0
          }
        })
      }

      commit('setReportListDataTable', items)
      resolve()
    }, 1000)
  }
}
