export default {
  drawer: null,
  color: 'primary',
  image: '/assets/img/sidebar-2.jpg',
  sidebarBackgroundColor: 'rgba(27, 27, 27, 0.74)'
}
