import axios from 'axios'

export default {
  async addTag ({ rootState, commit }, tag) {
    let config = {
      headers: { 'Authorization': 'Token ' + rootState.user.token }
    }
    return axios.post('/tag/', tag, config)
  },
  async getTagList ({ rootState, commit }) {
    let config = {
      headers: { 'Authorization': 'Token ' + rootState.user.token }
    }
    return axios.get('/tag/', config).then(response => {
      commit('setTagList', response.data)
      commit('setTagListDataTable', response.data)
      commit('setTotalTag', response.data.length)
    })
  },
  async tagPaginationHandler ({ state, commit }) {
    return new Promise((resolve, reject) => {
      const { sortBy, descending } = state.tagPagination
      let items = state.tagList

      if (sortBy) {
        items = items.sort((a, b) => {
          const sortA = a[sortBy]
          const sortB = b[sortBy]

          if (descending) {
            if (sortA < sortB) return 1
            if (sortA > sortB) return -1
            return 0
          } else {
            if (sortA < sortB) return -1
            if (sortA > sortB) return 1
            return 0
          }
        })
      }

      commit('setTagListDataTable', items)
      resolve()
    }, 1000)
  },
  async deleteTag ({ rootState, commit }, tagId) {
    let config = {
      headers: { 'Authorization': 'Token ' + rootState.user.token }
    }
    return axios.delete(`/tag/${tagId}/`, config)
  }
}
