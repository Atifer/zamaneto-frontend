export default {
  tagList: null,
  tagPagination: {
    descending: true,
    page: 1,
    rowsPerPage: 5,
    sortBy: 'start',
    totalItems: 5
  },
  tagListDataTable: null
}
