import { set } from '@/utils/vuex'

export default {
  setTagList: set('tagList'),
  setTagPagination: set('tagPagination'),
  setTagListDataTable: set('tagListDataTable'),
  setTotalTag (state, totalTag) {
    state.tagPagination.totalItems = totalTag
  }
}
