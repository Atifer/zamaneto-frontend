// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'

// Components
import './components'

// Plugins
import './plugins'
import VuePersianDatetimePicker from 'vue-persian-datetime-picker'

// Application imports
import VueRouter from 'vue-router'
import RouterPrefetch from 'vue-router-prefetch'
import App from './App'
import i18n from '@/i18n'
import router from '@/router'
import store from '@/store'
// Sync router with store
import { sync } from 'vuex-router-sync'
Vue.component('date-picker', VuePersianDatetimePicker)

Vue.use(VueRouter)
Vue.use(RouterPrefetch)
// Sync store with router
sync(store, router)

Vue.config.productionTip = false

Vue.prototype.$docTitle = 'Zamaneto'
document.title = Vue.prototype.$docTitle

/* eslint-disable no-new */
new Vue({
  i18n,
  router,
  store,
  render: h => h(App)
}).$mount('#app')
